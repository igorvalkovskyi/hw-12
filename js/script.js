//setTimeout дозволяє нам запускати функцію один раз через певний інтервал часу. setInterval дозволяє нам запускати функцію багаторазово, починаючи через певний інтервал часу, а потім постійно повторює
//Якщо передати нульову затримку,код спрацює,але не миттєво(лише після виконання всього коду сторінки) 
//Тому що непотрібні цикли будуть загружать сторінку,а це в свою чергу буде впливати на швидкість роботи
let images = document.querySelectorAll("#images-wrapper img");
let i = 0;
function nextImg(){
     if(i === images.length - 1){
        images[i].style.display = "none";
        i = 0;
        images[0].style.display = "block"; 
     } else {
        images[i].style.display = "none";
        images[i + 1].style.display = "block";
        i++;
     }
}
let stopButton = setInterval(nextImg, 3000)


function StopFunction() {
   clearInterval(stopButton);
 }

 function startFunction(){
   setInterval(nextImg, 3000);
   }